package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.pojos.Vehicle;
import com.app.service.IVehicleService;

@CrossOrigin
@RestController
@RequestMapping("/vehicleUsage") 
public class VehicleUsageController {

	@Autowired
	private IVehicleService service;
	
	
	@PostMapping(value="/addUsage")
	public ResponseEntity<Void> registerVehicle(@RequestBody Vehicle v)
	{
		System.out.println("From Session-------------------");
		System.out.println(v);
		service.addVehicle(v);
		
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}
}
