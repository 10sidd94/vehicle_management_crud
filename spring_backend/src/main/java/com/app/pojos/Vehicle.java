package com.app.pojos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class Vehicle {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="vId")
	private int vehicleId;

	@Column(length=30)
	@Enumerated(EnumType.STRING)
	private VehicleType type;

	@Column(length=20,unique = true)
	private String chessisNumber;

	@Column(length=4)
	private int modelYear;

	@Lob
	private byte[] vImage;

	private boolean disableVehicle;

	public Vehicle() {

		System.out.println("in default constriuctor of " + getClass().getName());
	}

	public Vehicle(int vehicleId, VehicleType type, String chessisNumber, int modelYear, byte[] vImage,
			boolean disableVehicle) {

		this.vehicleId = vehicleId;
		this.type = type;
		this.chessisNumber = chessisNumber;
		this.modelYear = modelYear;
		this.vImage = vImage;
		this.disableVehicle = disableVehicle;
	}

	public int getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(int vehicleId) {
		this.vehicleId = vehicleId;
	}

	public VehicleType getType() {
		return type;
	}

	public void setType(VehicleType type) {
		this.type = type;
	}

	public String getChessisNumber() {
		return chessisNumber;
	}

	public void setChessisNumber(String chessisNumber) {
		this.chessisNumber = chessisNumber;
	}

	public int getModelYear() {
		return modelYear;
	}

	public void setModelYear(int modelYear) {
		this.modelYear = modelYear;
	}

	public byte[] getvImage() {
		return vImage;
	}

	public void setvImage(byte[] vImage) {
		this.vImage = vImage;
	}

	public boolean isDisableVehicle() {
		return disableVehicle;
	}

	public void setDisableVehicle(boolean disableVehicle) {
		this.disableVehicle = disableVehicle;
	}

	@Override
	public String toString() {
		return "Vehicle [vehicleId=" + vehicleId + ", type=" + type + ", chessisNumber=" + chessisNumber
				+ ", modelYear=" + modelYear + " disableVehicle=" + disableVehicle + "]";
	}

}
