package com.app.pojos;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class VehicleBooking {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="bId")
	private int bookingId;
	
	//foriegn key
	//private int vId;
	@OneToOne(fetch = FetchType.LAZY)
	private Vehicle vId;
	
	
	private LocalDate BookingDate;
	
	@Column(length=30)
	private String fromPlace;
	
	@Column(length=30)
	private String toPalce;

	public VehicleBooking() {

		System.out.println("in default constriuctor of " + getClass().getName());
	}

	

	public VehicleBooking(int bookingId, Vehicle vId, LocalDate bookingDate, String fromPlace, String toPalce) {
		
		this.bookingId = bookingId;
		this.vId = vId;
		BookingDate = bookingDate;
		this.fromPlace = fromPlace;
		this.toPalce = toPalce;
	}



	public int getBookingId() {
		return bookingId;
	}

	public void setBookingId(int bookingId) {
		this.bookingId = bookingId;
	}

	public Vehicle getvId() {
		return vId;
	}

	public void setvId(Vehicle vId) {
		this.vId = vId;
	}

	public String getFromPlace() {
		return fromPlace;
	}

	public void setFromPlace(String fromPlace) {
		this.fromPlace = fromPlace;
	}

	public String getToPalce() {
		return toPalce;
	}

	public void setToPalce(String toPalce) {
		this.toPalce = toPalce;
	}
	
	

	public LocalDate getBookingDate() {
		return BookingDate;
	}



	public void setBookingDate(LocalDate bookingDate) {
		BookingDate = bookingDate;
	}



	@Override
	public String toString() {
		return "VehicleBooking [bookingId=" + bookingId + ", vId=" + vId + ", BookingDate=" + BookingDate
				+ ", fromPlace=" + fromPlace + ", toPalce=" + toPalce + "]";
	}




	
	
	
	
}
